package backend.order;

public interface OrderService {

    Order create(OrderDto orderDto);
}
